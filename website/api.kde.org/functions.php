<?php

  function apidox($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidoxqch($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a>";
    print '&nbsp;[<a href="qch/' . $module . '-' . $v . '.qch">qch</a>]</li>' . "\n\n";
  }

  function apidoxmanonly($name) {
    print '<li>';
    print $name;
    print '&nbsp;[<a href="man/' . $name . '-man.tar.bz2">man</a>]</li>' . "\n\n";
  }

  function frameworksqchman($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download frameworks ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for frameworks ' . $module . '">';
    print $module . "</a>";
    print '&nbsp;[<a href="qch/' . $module . '-' . $v . '.qch">qch</a>][<a href="man/' . $module . '-' . $v . '-man.tar.bz2">man</a>]</li>' . "\n\n";
  }

  function apidoxqchman($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a>";
    print '&nbsp;[<a href="qch/' . $module . '-' . $v . '.qch">qch</a>][<a href="man/' . $module . '-' . $v . '-man.tar.bz2">man</a>]</li>' . "\n\n";
  }

  function apidox2($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $v . '-apidocs/' . $module . '/html/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidox2qch($v,$module) {
    print '<li><a href="' . $v . '-api/' . $v . '-apidocs/' . $module . '/html/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a>";
    print '&nbsp;[<a href="qch/' . $module . '-' . $v . '.qch">qch</a>]</li>' . "\n\n";
  }

  function apidox3($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidox4($v,$module) {
    print '<li><a href="' . $v . '-api/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

?>
