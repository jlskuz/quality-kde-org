<?php
$page_title = "Website Settings";
$site_root = "../";
$site_external = true;
include "header.inc";
?>

<p>
Welcome to the KDE Website Settings Dialog. This page enables you to customize
the appearance of all KDE.org subsites that use the new layout.
</p>

<p>
<b style="color: red">NOTE:</b> This feature requires cookies to be enabled!
If switching the style doesn't seem to work, hit reload.
If you are using Konqueror or Mozilla, you can also temporarily switch the style
sheet in the View menu.
</p>

<form action="switch.php" method="post" name="Theme" id="Theme">
 <p>
  <label for="site_style">Choose a color style: </label><br />
  <select size="1" name="site_style" id="site_style" class="settings" title="Choose between different color schemes.">
   <option value="blackwhite"<?php if($site_style == "blackwhite") echo " selected=\"selected\"";?>>Black and white</option>
   <option value="bluemellow"<?php if($site_style == "bluemellow") echo " selected=\"selected\"";?>>Blue, mellow</option>
   <option value="classic"<?php if($site_style == "classic") echo " selected=\"selected\"";?>>Classic Blue</option>
   <option value="largescreen"<?php if($site_style == "largescreen") echo " selected=\"selected\"";?>>KDE Window Colors (Large Screen)</option>
   <option value="windowcolors"<?php if($site_style == "windowcolors") echo " selected=\"selected\"";?>>KDE Window Colors</option>
   <option value="orange"<?php if($site_style == "orange") echo " selected=\"selected\"";?>>Orange</option>
   <option value="yellow"<?php if($site_style == "yellow") echo " selected=\"selected\"";?>>Yellow</option>
  </select>
  <input type="submit" value="Change Style" />
 </p>
</form>

<?php
include "footer.inc";
?>
