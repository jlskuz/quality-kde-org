<form method="get" name="Searchform" action="/media/search.php">
<span class="invisible"><label for="Input" accesskey="4"><?php i18n("Search:")?></label></span>
<input type="text" size="10" name="q" id="Input" value="" /><br />
<span class="invisible"><label for="Select"> in </label></span>
<select size="1" name="Select" id="Select" title="<?php i18n("Select what or where you want to search")?>">

<option value="kde.org" <?php if ($site != "developer") print 'selected="selected"'; ?>><?php i18n("kde.org")?></option>

<option value="developer.kde.org" <?php if ($site == "developer") print 'selected="selected"'; ?>><?php i18n("developer.kde.org")?></option>

<option value="kdelook"><?php i18n("kde-look.org")?></option>
<option value="kdeapps"><?php i18n("Applications")?></option>
<option value="kdedoc"><?php i18n("Documentation")?></option>
</select><br />
<input type="submit" value=" <?php i18n("Search")?> " name="Button" id="searchButton" />
</form>
