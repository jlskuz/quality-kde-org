<?php

/**
 * Menu code for developer.kde.org, this is the developer.kde.org/media version
 * Written by Christoph Cullmann <cullmann@kde.org>
 * Based on the menu code written by Simon Edwards <simon@simonzone.com>
 */

class BaseMenu
{
  var $menu_root;
  var $menu_baseurl;
  var $items = array();
  var $current_relativeurl;

  /**
   * constructs base menu, only used in kde_menu methode
   */
  function BaseMenu ($menu_root, $menu_baseurl, $current_relativeurl)
  {
    $this->menu_root = $menu_root;
    $this->menu_baseurl = $menu_baseurl;
    $this->current_relativeurl = $current_relativeurl;

    $this->appendSection("Home", "", ""); // dummy entry, needed for startpage !

    if (file_exists ($this->menu_root."/menu.inc") && is_readable ($menu_root."/menu.inc"))
      include ($menu_root."/menu.inc");
  }

  /**
   * Append a section with it's corresponding dir name, empty dirname indicates the "home" section !
   */
  function appendSection ($name, $dir, $comment)
  {
    $section = new MenuSection ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl, $name, $dir, $comment);
    array_push($this->items,$section);

    return $section;
  }

  /**
   * Return the current active section of the menu
   * default to the first section in the menu if no other marked active
   * this is the case for the index.php for example !
   */
  function activeSection ()
  {
    for($i=0; $i < count($this->items); $i++)
    {
      if ($this->items[$i]->active)
        return $i;
    }

    return 0;
  }

  /**
   * returns the link list of the known sections, used in the nav_header_sections div
   * active section is bold !
   */
  function sections ()
  {
    $s = "";

    $actives = $this->activeSection ();

    for($i=0; $i < count($this->items); $i++)
    {

      if ($i == $actives)
        $s .= "<b>";

      $s .= $this->items[$i]->sectionLink ($actives == $i);

      if ($i == $actives)
        $s .= "</b>";

      if (($i+1) < count($this->items))
        $s .= " :: ";
    }

    print $s;
  }

  /**
   * shows the current active menu, which means:
   * a) the sections overview, if you are at Home
   * b) the menu of the current active section otherwise
   */
  function currentMenu ($forceoverview = false)
  {
    $actives = $this->activeSection ();

    if ($forceoverview || ($actives == 0))
    {
      print "<h2>Sitemap</h2>\n";

      print "<ul>\n";

      for($i=0; $i < count($this->items); $i++)
        print "<li>".$this->items[$i]->sectionLink ()."</li>\n";

      print "</ul>\n";
    }
    else
    {
      $this->items[$actives]->showMenu ();
    }
  }

  /**
   * shows a overview about the currentMenu
   * a) the sections overview list, if you are at Home
   * b) the overview of the current active section otherwise
   */
  function currentOverview ()
  {
    $actives = $this->activeSection ();

    if ($actives == 0)
    {
      print "<ul>\n";

      for($i=1; $i < count($this->items); $i++)
      {
        $s = "";

        $s .= $this->items[$i]->sectionLink ()."<br />\n";
        $s .= $this->items[$i]->comment;

        print "<li><p>".$s."</p><p /></li>\n";
      }

      print "</ul>\n";
    }
    else
    {
      $this->items[$actives]->showOverview ();
    }
  }

  function showItem($link)
  {
    $this->items[$this->activeSection()]->showItem($link);
  }

  function showLocation ()
  {
    print 'Location'.': <a href="'.($this->menu_baseurl).'" accesskey="1">'.'Developer Home'.'</a>';

    if(count($this->items))
    {
      for($i=0; $i < count($this->items); $i++)
      {
       $this->items[$i]->showLocation ();
      }
    }
  }
}

class MenuSection
{
  var $menu_root;
  var $menu_baseurl;
  var $name;
  var $items = array();
  var $current_relativeurl;
  var $active = false;
  var $dir;
  var $comment;
  var $comments = array ();

  /**
   * constructs the section
   */
  function MenuSection ($menu_root, $menu_baseurl, $current_relativeurl, $name, $dir, $comment)
  {
    $this->menu_root = $menu_root."/".$dir;

    if ($menu_baseurl != "/")
    {
      $this->menu_baseurl = $menu_baseurl."/".$dir;
    }
    else
    {
      $this->menu_baseurl = $menu_baseurl.$dir;
    }

    $this->name = $name;
    $this->dir = $dir;
    $this->items = array();
    $this->comment = $comment;

    if (strncmp ($dir."/", $current_relativeurl, strlen ($dir."/")) == 0)
    {
      $this->current_relativeurl = substr ($current_relativeurl, strlen($dir."/"), strlen($current_relativeurl)-strlen($dir."/"));
      $this->active = true;

      if (file_exists ($this->menu_root."/menu.inc") && is_readable ($this->menu_root."/menu.inc"))
        include ($this->menu_root."/menu.inc");
    }
    else
    {
      $this->current_relativeurl = "";
    }
  }

  /**
   * append a link aka MenuItem
   */
  function appendLink ($name, $file, $comment = "", $relative_link = true)
  {
    $item = new MenuItem ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl, $name, $file, $relative_link);
    array_push($this->items,$item);
    array_push($this->comments, $comment);

    return $item;
  }

  /**
   * append a directory aka Menu
   */
  function appendDir ($name, $dir, $comment = "")
  {
    $menu = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl, $name, $dir);
    array_push($this->items,$menu);
    array_push($this->comments, $comment);

    return $menu;
  }

  /**
   * show the menu of this section
   */
  function showMenu ()
  {
    print "<h2>$this->name</h2>\n";
    print "<ul>\n";

    print "<li><a href=\"$this->menu_baseurl/\">Overview</a></li>";

    for($i=0; $i < count($this->items); $i++)
    {
      print "<li>\n";
      $this->items[$i]->show ();
      print "</li>\n";
    }

    print "</ul>\n";
  }

  /**
   * generate a link to this section
   */
  function sectionLink ($active = false)
  {
    if ($active)
      $s = "id=\"nav_header_sections_active\"";
    else
      $s = "";

    return "<a $s href=\"".$this->menu_baseurl."\">".$this->name."</a>";
  }

  function showOverview ()
  {
    print "<ul>\n";

    for($i=0; $i < count($this->items); $i++)
    {
      $s = "";

      $s .= $this->items[$i]->getLink ()."<br />\n";
      $s .= $this->comments[$i];

      print "<li><p>".$s."</p><p /></li>\n";
    }

    print "</ul>\n";
  }

  function showItem($link)
  {
    for($i=0; $i < count($this->items); $i++)
    {
      $l = $this->items[$i]->getLinkFile();
      if ( $l == $link )
      {
        $s = "";

        $s .= $this->items[$i]->getLink()."<br />\n";
        $s .= $this->comments[$i];

        print "<li>".$s."</li>\n";
      }
    }
  }

  function showLocation ()
  {
    if ($this->active)
    {
      print ' / <a href="'.($this->menu_baseurl).'/" accesskey="1">'.$this->name.'</a>';

      for($i=0; $i < count($this->items); $i++)
      {
        $this->items[$i]->showLocation ();
      }
    }
  }
}

class Menu {
  var $menu_root;
  var $menu_baseurl;
  var $items;
  var $name;
  var $dir;
  var $current_relativeurl;
  var $active = false;

  function Menu($menu_root, $menu_baseurl, $current_relativeurl, $name, $dir)
  {
    $this->menu_root = $menu_root."/".$dir;

    if ($menu_baseurl != "/")
    {
      $this->menu_baseurl = $menu_baseurl."/".$dir;
    }
    else
    {
      $this->menu_baseurl = $menu_baseurl.$dir;
    }

    $this->name = $name;
    $this->dir = $dir;
    $this->items = array();

    if (strncmp ($dir."/", $current_relativeurl, strlen ($dir."/")) == 0)
    {
      $this->current_relativeurl = substr ($current_relativeurl, strlen($dir), strlen($current_relativeurl)-strlen($dir));
      $this->active = true;

      if (file_exists ($this->menu_root."/menu.inc") && is_readable ($this->menu_root."/menu.inc"))
        include ($this->menu_root."/menu.inc");
    }
    else
    {
      $this->current_relativeurl = "";
    }
  }

  function appendLink ($name, $file, $relative_link = true)
  {
    $item = new MenuItem ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl, $name, $file, $relative_link);
    array_push($this->items,$item);

    return $item;
  }

  function appendDir ($name, $dir)
  {
    $menu = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl, $name, $dir, true);
    array_push($this->items,$menu);

    return $menu;
  }

  function show ()
  {
    print '<a href="'.($this->menu_baseurl).'/">'.$this->name."</a>\n";

    if (count($this->items) > 0)
    {
      print "<ul>\n";

      for($i=0; $i < count($this->items); $i++)
      {
        print "<li>\n";
        $this->items[$i]->show ();
        print "</li>\n";
      }

      print "</ul>\n";
    }
  }

  function getLink ()
  {
    return '<a href="'.($this->menu_baseurl).'/">'.$this->name."</a>\n";
  }

  function showLocation ()
  {
    if ($this->active)
    {
      print ' / <a href="'.($this->menu_baseurl).'/">'.$this->name.'</a>';

      if (count($this->items) > 0)
      {
        for($i=0; $i < count($this->items); $i++)
        {
          $this->items[$i]->showLocation ();
        }
      }
    }
  }
}

class MenuItem
{
  var $menu_root;
  var $menu_baseurl;
  var $name;
  var $file;
  var $relative_link;
  var $active = false;
  var $u;

  function MenuItem ($menu_root, $menu_baseurl, $current_relativeurl, $name, $file, $relative_link)
  {
    $this->menu_root = $menu_root;
    $this->menu_baseurl = $menu_baseurl;
    $this->name = $name;
    $this->file = $file;
    $this->relative_link = $relative_link;


    if ($file <> "")
    if ($relative_link && (substr_count($current_relativeurl, $file) == 1))
      $this->active = true;
  }

  function show ()
  {
    if ($this->relative_link)
    {
      if ($this->menu_baseurl != "/")
        $url = $this->menu_baseurl."/".$this->file;
      else
        $url = $this->menu_baseurl.$this->file;
    }
    else
      $url = $this->file;

    print '<a href="'.$url.'">'.$this->name."</a>\n";
  }

  function getLinkFile()
  {
    return $this->file;
  }

  function getLink ()
  {
    if ($this->relative_link)
    {
      if ($this->menu_baseurl != "/")
        $url = $this->menu_baseurl."/".$this->file;
      else
        $url = $this->menu_baseurl.$this->file;
    }
    else
      $url = $this->file;

    return '<a href="'.$url.'">'.$this->name."</a>\n";
  }

  function showLocation ()
  {
    if ($this->active)
      print " / ".$this->name;
  }
}

?>
