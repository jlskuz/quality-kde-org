<?php

/**
 * Written by Chris Howells <howells@kde.org>
 * Heavilty based on Dirk Mueller's 404 handler page
 */

class Handler404
{
	var $mapper = array();
	var $dirmapper = array ();
	var $address;

	function Handler404()
	{
	}

	function execute()
	{
		if (strstr($_SERVER['REQUEST_URI'], ".html") && !strstr($_SERVER['REQUEST_URI'], "..")
		&& (file_exists("../" . str_replace(".html", ".php", $_SERVER['REQUEST_URI']))
		|| file_exists("./" . str_replace(".html", ".php", $_SERVER['REQUEST_URI']))))
		{
			header("Location: " . str_replace(".html", ".php", $_SERVER['REQUEST_URI']));
			exit();
		}

		if (strstr($_SERVER['REQUEST_URI'], ".htm") && !strstr($_SERVER['REQUEST_URI'], "..")
		&& (file_exists("../" . str_replace(".htm", ".php", $_SERVER['REQUEST_URI']))
		|| file_exists("./" . str_replace(".htm", ".php", $_SERVER['REQUEST_URI']))))
		{
			header("Location: " . str_replace(".htm", ".php", $_SERVER['REQUEST_URI']), true, 301);
			exit();
		}

		if (strstr($_SERVER['REQUEST_URI'], ".phtml") && !strstr($_SERVER['REQUEST_URI'], "..")
                && (file_exists("../" . str_replace(".phtml", ".php", $_SERVER['REQUEST_URI']))
		|| file_exists("./" . str_replace(".phtml", ".php", $_SERVER['REQUEST_URI']))))
                {
                        header("Location: " . str_replace(".phtml", ".php", $_SERVER['REQUEST_URI']), true, 301);
                        exit();
                }

		$uri = $_SERVER['REQUEST_URI'];
		$server = $_SERVER['SERVER_NAME'];

		# remove trailing slash for mapping
      		if (strlen($uri) > 1 && $uri[strlen($uri)-1] == "/")
		{
			$uri = substr($uri, 0, strlen($uri)-1);
		}

		if ($this->mapper[$uri])
		{
			header("Location: " . $this->mapper[$uri], true, 301);
			exit();
		}
		
		// last try, map whole dirs
		foreach ($this->dirmapper as $from => $to) {
                    if ( (strncmp ($from, $_SERVER['REQUEST_URI'], strlen($from)) == 0)
                          && ( (strlen($from) == strlen($_SERVER['REQUEST_URI']))
                               || ($_SERVER['REQUEST_URI'][strlen($from)] == "/") ) )
                    {
                      header("Location: " . str_replace($from, $to, $_SERVER['REQUEST_URI']), true, 301);
                      exit();
                    }
                }
	
		$this->showError();
	}

	function add($old_url, $new_url)
	{
		$this->mapper[$old_url] = $new_url;
	}
	
	function addDir ($old_url, $new_url)
	{
		$this->dirmapper[$old_url] = $new_url;
	}

	function mail($address)
	{
		$this->address = $address;
	}

	function showError()
	{
		print "<html><head><title>404 Not found</title></head>\n";
		print "<body>\n";
		print "<h1>Not Found</h1>\n";
		print "<p>The requested URL ". $_SERVER['REQUEST_URI'] . " was not found on this server.</p>\n";
		print "</body>\n";
		print "</html>\n";

		if (isset($this->address))
		{
			if (!strstr($_SERVER['REQUEST_URI'], "favicon.ico") && !strstr($_SERVER['REQUEST_URI'], "kdenews.rdf")
			&& !strstr($_SERVER['REQUEST_URI'], "root.exe") && !strstr($_SERVER['REQUEST_URI'], "cmd.exe")
			&& !strstr($_SERVER['REQUEST_URI'], "/es/") && !strstr($_SERVER['REQUEST_URI'], "/fr/")
			&& !strstr($_SERVER['REQUEST_URI'], "/people/images/") && !strstr($_SERVER['REQUEST_URI'], "/international/"))
			{
				 mail($this->address, "404", "404 while requesting " . $_SERVER['REQUEST_URI'] . " from " . $_SERVER['HTTP_REFERER'] . " on host $server\n");
			}
		}
	}
}

