<?php

/**
 * Written by Chris Howells <howells@kde.org>
 * Based on code by Christoph Cullmann <cullmann@kde.org>
 * Based on the cool menu code from usability.kde.org written by Simon Edwards <simon@simonzone.com>
 */

class HotSpot
{
	var $items = array();
	var $item;

	function Hotspot()
	{
	}

	function add($image, $url, $description = "")
	{
		$item = new HotspotItem($image, $url, $description);
		array_push($this->items, $item);
	}

	function show()
	{
		if (count($this->items))
		{
			$number = rand(0, count($this->items) - 1);
			$this->items[$number]->show();
		}
	}
}

class HotspotItem
{
	var $image;
	var $url;
	var $description;

	function HotspotItem($image, $url, $description)
	{
		$this->image = $image;
		$this->url = $url;
		$this->description = $description;
	}

	function show()
	{
		echo "<div id=\"hotspot\">\n";
		echo "<a href=\"$this->url\"><img src=\"$this->image\" vspace=\"3\" align=\"middle\" alt=\"Hotspot\" border=\"0\" /></a>\n";
		if ($this->description != "")
		{
			echo "<br />\n";
			echo "<a href=\"$this->url\">$this->description</a>\n";
			echo "<br />\n";
		}
		echo "</div>\n";
	}
}
