<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
# Copyright (C) 2007 by Frerich Raabe <raabe@kde.org>
#
# This file generates a graphic for a 0-100 percent rating.
# Taken from one of the comments on
#     http://us3.php.net/manual/en/function.imagefilledrectangle.php
#
# You can include that php page in your html as you would an image:
#     <IMG SRC="ratingpng.php?rating=25.2" border="0">

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

function drawRating($rating) {
	if (isset($_GET['width'])) {
		$width = $_GET['width'];
	} else {
		$width = 102;
	}

	if (isset($_GET['height'])) {
		$height = $_GET['height'];
	} else {
		$height = 10;
	}

	if ($width > 1024) {
		$width = 1024;
	}
	if ($height > 1024) {
		$height = 1024;
	}
 
	if (!is_numeric($rating)) {
	    $rating = 0;
	}
	if ($rating < 0) {
	    $rating = 0;
	}
	if ($rating > 100) {
	    $rating = 100;
	}
	$ratingbar = $rating/100.0*$width;
 
	$image = imagecreate($width,$height);
	$back = ImageColorAllocate($image,255,255,255);
	$fill = ImageColorAllocate($image,208,184,140);

	ImageFilledRectangle($image,0,0,$ratingbar,$height-1,$fill);
	imagePNG($image);
	imagedestroy($image);
}
 
Header("Content-type: image/png");
if (isset($_GET['rating'])) {
    drawRating($_GET['rating']);
} else {
    drawRating(0);
}
?>
