<h1>KDE Usability Watchdog</h1>
<p>The KDE Usability Watchdog looks over applications which are part of
the <a href="http://www.kde.org">KDE</a> project, searching for
usability issues.</p>
