<h1>KDE Source Code Checker</h1>

<p>The Source Code Checker (also known as 'Krazy') scans KDE source code
looking for issues that should be fixed for reasons of policy, good coding
practice, optimization, or any other good reason.  In typical use, this tool
simply counts up the issues and provides the line numbers where those issues
occurred in each file processed.</p>

<p>We try to eliminate false positives as much as we can while keeping the
checks fast.  Please contact <a href="mailto:winter@kde.org">Allen</a>
if you encounter false positives, have suggestions to improve this service,
or would like to contribute code checker plugins.
</p>

<p>Whenever possible, <b>check</b> with the respective maintainers before making
extensive or intrusive changes to their code.
</p>

<p><a href="http://community.kde.org/Guidelines_and_HOWTOs/Code_Checking">Learn more about Code Checking and Krazy.</a></p>
