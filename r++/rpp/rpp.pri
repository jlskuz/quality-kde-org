
INCLUDEPATH += $$PWD ../shared

HEADERS += \
    $$PWD/pp-cctype.h \
    $$PWD/pp-engine.h \
    $$PWD/pp-fwd.h \
    $$PWD/pp-internal.h \
    $$PWD/pp-macro-expander.h \
    $$PWD/pp-scanner.h \
    $$PWD/pp-symbol.h \
    $$PWD/pp-engine-bits.h \
    $$PWD/pp-environment.h \
    $$PWD/pp.h \
    $$PWD/pp-iterator.h \
    $$PWD/pp-macro.h \
    $$PWD/pp-string.h
