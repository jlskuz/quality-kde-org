/*
    Copyright (C) 2002-2007 Roberto Raggi <roberto@kdevelop.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef AST_H
#define AST_H

#include "rpp-memory-pool.h"
#include "rpp-list.h"
#include "rpp-ast-fwd.h"
#include "rpp-ast-visitor.h"

#define DECLARE_AST_NODE(k) \
    enum { __node_kind = Kind_##k };

namespace rpp {

class AST
{
public:
  enum NODE_KIND
    {
      Kind_UNKNOWN = 0,

      Kind_AccessSpecifier,
      Kind_AsmDefinition,
      Kind_BaseClause,
      Kind_BaseSpecifier,
      Kind_BinaryExpression,
      Kind_CastExpression,
      Kind_ClassMemberAccess,
      Kind_ClassSpecifier,
      Kind_CompoundStatement,
      Kind_Condition,
      Kind_ConditionalExpression,
      Kind_CppCastExpression,
      Kind_CtorInitializer,
      Kind_DeclarationStatement,
      Kind_Declarator,
      Kind_DeleteExpression,
      Kind_DoStatement,
      Kind_ElaboratedTypeSpecifier,
      Kind_EnumSpecifier,
      Kind_Enumerator,
      Kind_ExceptionSpecification,
      Kind_ExpressionOrDeclarationStatement,
      Kind_ExpressionStatement,
      Kind_ForStatement,
      Kind_FunctionCall,
      Kind_FunctionDefinition,
      Kind_IfStatement,
      Kind_IncrDecrExpression,
      Kind_InitDeclarator,
      Kind_Initializer,
      Kind_InitializerClause,
      Kind_LabeledStatement,
      Kind_LinkageBody,
      Kind_LinkageSpecification,
      Kind_MemInitializer,
      Kind_Name,
      Kind_Namespace,
      Kind_NamespaceAliasDefinition,
      Kind_NewDeclarator,
      Kind_NewExpression,
      Kind_NewInitializer,
      Kind_NewTypeId,
      Kind_Operator,
      Kind_OperatorFunctionId,
      Kind_ParameterDeclaration,
      Kind_ParameterDeclarationClause,
      Kind_PostfixExpression,
      Kind_PrimaryExpression,
      Kind_PtrOperator,
      Kind_PtrToMember,
      Kind_ReturnStatement,
      Kind_SimpleDeclaration,
      Kind_SimpleTypeSpecifier,
      Kind_SizeofExpression,
      Kind_StringLiteral,
      Kind_SubscriptExpression,
      Kind_SwitchStatement,
      Kind_TemplateArgument,
      Kind_TemplateDeclaration,
      Kind_TemplateParameter,
      Kind_ThrowExpression,
      Kind_TranslationUnit,
      Kind_TryBlockStatement,
      Kind_TypeId,
      Kind_TypeIdentification,
      Kind_TypeParameter,
      Kind_Typedef,
      Kind_UnaryExpression,
      Kind_UnqualifiedName,
      Kind_Using,
      Kind_UsingDirective,
      Kind_WhileStatement,
      Kind_WinDeclSpec,

      NODE_KIND_COUNT
    };

public:
  virtual ~AST () {}

  void accept (ASTVisitor *visitor, SemanticEnvironment *context)
  {
    if (visitor->preVisit (this, context))
      this->accept0 (visitor, context);

    visitor->postVisit (this, context);
  }

  static void acceptNode (AST *node, ASTVisitor *visitor, SemanticEnvironment *context)
  {
    if (! node)
      return;

    node->accept (visitor, context);
  }

  static void acceptToken (std::size_t , ASTVisitor *, SemanticEnvironment *) {}
  static void acceptTokens (const ListNode<std::size_t> *, ASTVisitor *, SemanticEnvironment *) {}

  template <typename _Tp> // ### remove me
  static void acceptNodes (const ListNode<_Tp*> *nodes, ASTVisitor *visitor, SemanticEnvironment *context)
  {
    if (!nodes)
      return;

    const ListNode<_Tp*>
      *it = nodes->toFront(),
      *end = it;

    do
      {
        acceptNode (it->element, visitor, context);
        it = it->next;
      }
    while (it != end);
  }

protected:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context) = 0;

public: // attributes
  int kind;
  std::size_t start_token;
  std::size_t end_token;
};

class TypeSpecifierAST: public AST
{
public:
  const ListNode<std::size_t> *cv;
};

class StatementAST: public AST
{
public:
};

class ExpressionAST: public AST
{
public:
};

class DeclarationAST: public AST
{
public:
};

class AccessSpecifierAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (AccessSpecifier)

  const ListNode<std::size_t> *specs;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class AsmDefinitionAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (AsmDefinition)

  const ListNode<std::size_t> *cv;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class BaseClauseAST: public AST // ### kill me
{
public:
  DECLARE_AST_NODE (BaseClause)

  const ListNode<BaseSpecifierAST*> *base_specifiers;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class BaseSpecifierAST: public AST
{
public:
  DECLARE_AST_NODE (BaseSpecifier)

  std::size_t virt;
  std::size_t access_specifier;
  NameAST *name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class BinaryExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (BinaryExpression)

  std::size_t op;
  ExpressionAST *left_expression;
  ExpressionAST *right_expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class CastExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (CastExpression)

  TypeIdAST *type_id;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ClassMemberAccessAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (ClassMemberAccess)

  std::size_t op;
  NameAST *name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ClassSpecifierAST: public TypeSpecifierAST
{
public:
  DECLARE_AST_NODE (ClassSpecifier)

  WinDeclSpecAST *win_decl_specifiers;
  std::size_t class_key;
  NameAST *name;
  BaseClauseAST *base_clause;
  const ListNode<DeclarationAST*> *member_specs;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class CompoundStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (CompoundStatement)

  const ListNode<StatementAST*> *statements;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ConditionAST: public AST
{
public:
  DECLARE_AST_NODE (Condition)

  TypeSpecifierAST *type_specifier;
  DeclaratorAST *declarator;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ConditionalExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (ConditionalExpression)

  ExpressionAST *condition;
  ExpressionAST *left_expression;
  ExpressionAST *right_expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class CppCastExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (CppCastExpression)

  std::size_t op;
  TypeIdAST *type_id;
  ExpressionAST *expression;
  const ListNode<ExpressionAST*> *sub_expressions;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class CtorInitializerAST: public AST
{
public:
  DECLARE_AST_NODE (CtorInitializer)

  std::size_t colon;
  const ListNode<MemInitializerAST*> *member_initializers;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class DeclarationStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (DeclarationStatement)

  DeclarationAST *declaration;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class DeclaratorAST: public AST
{
public:
  DECLARE_AST_NODE (Declarator)

  const ListNode<PtrOperatorAST*> *ptr_ops;
  DeclaratorAST *sub_declarator;
  NameAST *id;
  ExpressionAST *bit_expression;
  const ListNode<ExpressionAST*> *array_dimensions;
  ParameterDeclarationClauseAST *parameter_declaration_clause;
  const ListNode<std::size_t> *fun_cv;
  ExceptionSpecificationAST *exception_spec;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class DeleteExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (DeleteExpression)

  std::size_t scope_token;
  std::size_t delete_token;
  std::size_t lbracket_token;
  std::size_t rbracket_token;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class DoStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (DoStatement)

  StatementAST *statement;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ElaboratedTypeSpecifierAST: public TypeSpecifierAST
{
public:
  DECLARE_AST_NODE (ElaboratedTypeSpecifier)

  std::size_t type;
  NameAST *name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class EnumSpecifierAST: public TypeSpecifierAST
{
public:
  DECLARE_AST_NODE (EnumSpecifier)

  NameAST *name;
  const ListNode<EnumeratorAST*> *enumerators;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class EnumeratorAST: public AST
{
public:
  DECLARE_AST_NODE (Enumerator)

  std::size_t id;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ExceptionSpecificationAST: public AST
{
public:
  DECLARE_AST_NODE (ExceptionSpecification)

  std::size_t ellipsis;
  const ListNode<TypeIdAST*> *type_ids;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ExpressionOrDeclarationStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (ExpressionOrDeclarationStatement)

  StatementAST *expression;
  StatementAST *declaration;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ExpressionStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (ExpressionStatement)

  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class FunctionCallAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (FunctionCall)

  ExpressionAST *arguments;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class FunctionDefinitionAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (FunctionDefinition)

  const ListNode<std::size_t> *storage_specifiers;
  const ListNode<std::size_t> *function_specifiers;
  TypeSpecifierAST *type_specifier;
  InitDeclaratorAST *init_declarator;
  StatementAST *function_body;
  WinDeclSpecAST *win_decl_specifiers;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ForStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (ForStatement)

  StatementAST *init_statement;
  ConditionAST *condition;
  ExpressionAST *expression;
  StatementAST *statement;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class IfStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (IfStatement)

  ConditionAST *condition;
  StatementAST *statement;
  StatementAST *else_statement;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class IncrDecrExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (IncrDecrExpression)

  std::size_t op;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class InitDeclaratorAST: public AST
{
public:
  DECLARE_AST_NODE (InitDeclarator)

  DeclaratorAST *declarator;
  InitializerAST *initializer;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class InitializerAST: public AST
{
public:
  DECLARE_AST_NODE (Initializer)

  InitializerClauseAST *initializer_clause;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class InitializerClauseAST: public AST
{
public:
  DECLARE_AST_NODE (InitializerClause)

  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class LabeledStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (LabeledStatement)

  std::size_t label; // ### implement me

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class LinkageBodyAST: public AST
{
public:
  DECLARE_AST_NODE (LinkageBody)

  const ListNode<DeclarationAST*> *declarations;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class LinkageSpecificationAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (LinkageSpecification)

  std::size_t extern_type;
  LinkageBodyAST *linkage_body;
  DeclarationAST *declaration;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class MemInitializerAST: public AST
{
public:
  DECLARE_AST_NODE (MemInitializer)

  NameAST *initializer_id;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class NameAST: public AST
{
public:
  DECLARE_AST_NODE (Name)

  bool global; // ### global token
  const ListNode<UnqualifiedNameAST*> *qualified_names;
  UnqualifiedNameAST *unqualified_name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class NamespaceAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (Namespace)

  std::size_t namespace_name;
  LinkageBodyAST *linkage_body;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class NamespaceAliasDefinitionAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (NamespaceAliasDefinition)

  std::size_t namespace_name;
  NameAST *alias_name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class NewDeclaratorAST: public AST
{
public:
  DECLARE_AST_NODE (NewDeclarator)

  PtrOperatorAST *ptr_op;
  NewDeclaratorAST *sub_declarator;
  const ListNode<ExpressionAST*> *expressions;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class NewExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (NewExpression)

  std::size_t scope_token;
  std::size_t new_token;
  ExpressionAST *expression;
  TypeIdAST *type_id;
  NewTypeIdAST *new_type_id;
  NewInitializerAST *new_initializer;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class NewInitializerAST: public AST
{
public:
  DECLARE_AST_NODE (NewInitializer)

  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class NewTypeIdAST: public AST
{
public:
  DECLARE_AST_NODE (NewTypeId)

  TypeSpecifierAST *type_specifier;
  NewInitializerAST *new_initializer;
  NewDeclaratorAST *new_declarator;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class OperatorAST: public AST
{
public:
  DECLARE_AST_NODE (Operator)

  std::size_t op;
  std::size_t open;
  std::size_t close;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class OperatorFunctionIdAST: public AST
{
public:
  DECLARE_AST_NODE (OperatorFunctionId)

  OperatorAST *op;
  TypeSpecifierAST *type_specifier;
  const ListNode<PtrOperatorAST*> *ptr_ops;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ParameterDeclarationAST: public AST
{
public:
  DECLARE_AST_NODE (ParameterDeclaration)

  TypeSpecifierAST *type_specifier;
  DeclaratorAST *declarator;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ParameterDeclarationClauseAST: public AST
{
public:
  DECLARE_AST_NODE (ParameterDeclarationClause)

  const ListNode<ParameterDeclarationAST*> *parameter_declarations;
  std::size_t ellipsis;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class PostfixExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (PostfixExpression)

  TypeSpecifierAST *type_specifier;
  ExpressionAST *expression;
  const ListNode<ExpressionAST*> *sub_expressions;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class PrimaryExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (PrimaryExpression)

  StringLiteralAST *literal;
  std::size_t token;
  StatementAST *expression_statement;
  ExpressionAST *sub_expression;
  NameAST *name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class PtrOperatorAST: public AST
{
public:
  DECLARE_AST_NODE (PtrOperator)

  const ListNode<std::size_t> *cv;
  std::size_t op;
  PtrToMemberAST *mem_ptr;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class PtrToMemberAST: public AST // ### the AST?
{
public:
  DECLARE_AST_NODE (PtrToMember)

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ReturnStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (ReturnStatement)

  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class SimpleDeclarationAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (SimpleDeclaration)

  const ListNode<std::size_t> *storage_specifiers;
  const ListNode<std::size_t> *function_specifiers;
  TypeSpecifierAST *type_specifier;
  const ListNode<InitDeclaratorAST*> *init_declarators;
  WinDeclSpecAST *win_decl_specifiers; // ### check the order

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class SimpleTypeSpecifierAST: public TypeSpecifierAST
{
public:
  DECLARE_AST_NODE (SimpleTypeSpecifier)

  const ListNode<std::size_t> *integrals;
  std::size_t type_of;
  TypeIdAST *type_id;
  ExpressionAST *expression;
  NameAST *name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class SizeofExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (SizeofExpression)

  std::size_t sizeof_token;
  TypeIdAST *type_id;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class StringLiteralAST: public AST
{
public:
  DECLARE_AST_NODE (StringLiteral)

  const ListNode<std::size_t> *literals;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class SubscriptExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (SubscriptExpression)

  ExpressionAST *subscript;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class SwitchStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (SwitchStatement)

  ConditionAST *condition;
  StatementAST *statement;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TemplateArgumentAST: public AST
{
public:
  DECLARE_AST_NODE (TemplateArgument)

  TypeIdAST *type_id;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TemplateDeclarationAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (TemplateDeclaration)

  std::size_t exported;
  const ListNode<TemplateParameterAST*> *template_parameters;
  DeclarationAST* declaration;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TemplateParameterAST: public AST
{
public:
  DECLARE_AST_NODE (TemplateParameter)

  TypeParameterAST *type_parameter;
  ParameterDeclarationAST *parameter_declaration;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class ThrowExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (ThrowExpression)

  std::size_t throw_token;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TranslationUnitAST: public AST
{
public:
  DECLARE_AST_NODE (TranslationUnit)

  const ListNode<DeclarationAST*> *declarations;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TryBlockStatementAST: public StatementAST // ### implement me
{
public:
  DECLARE_AST_NODE (TryBlockStatement)

protected:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TypeIdAST: public AST
{
public:
  DECLARE_AST_NODE (TypeId)

  TypeSpecifierAST *type_specifier;
  DeclaratorAST *declarator;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TypeIdentificationAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (TypeIdentification)

  std::size_t typename_token;
  NameAST *name;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TypeParameterAST: public AST
{
public:
  DECLARE_AST_NODE (TypeParameter)

  std::size_t type;
  NameAST *name;
  TypeIdAST *type_id;
  const ListNode<TemplateParameterAST*> *template_parameters;
  NameAST *template_name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class TypedefAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (Typedef)

  TypeSpecifierAST *type_specifier;
  const ListNode<InitDeclaratorAST*> *init_declarators;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class UnaryExpressionAST: public ExpressionAST
{
public:
  DECLARE_AST_NODE (UnaryExpression)

  std::size_t op;
  ExpressionAST *expression;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class UnqualifiedNameAST: public AST
{
public:
  DECLARE_AST_NODE (UnqualifiedName)

  std::size_t tilde;
  std::size_t id;
  OperatorFunctionIdAST *operator_id;
  const ListNode<TemplateArgumentAST*> *template_arguments;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class UsingAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (Using)

  std::size_t type_name;
  NameAST *name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class UsingDirectiveAST: public DeclarationAST
{
public:
  DECLARE_AST_NODE (UsingDirective)

  NameAST *name;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class WhileStatementAST: public StatementAST
{
public:
  DECLARE_AST_NODE (WhileStatement)

  ConditionAST *condition;
  StatementAST *statement;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

class WinDeclSpecAST: public AST
{
public:
  DECLARE_AST_NODE (WinDeclSpec)

  std::size_t specifier;
  std::size_t modifier;

public:
  virtual void accept0 (ASTVisitor *visitor, SemanticEnvironment *context);
};

template <class _Tp>
_Tp *CreateNode (pool *memory_pool)
{
  void *where = reinterpret_cast<_Tp*> (memory_pool->allocate (sizeof (_Tp)));
  _Tp *node = new (where) _Tp ();
  node->kind = _Tp::__node_kind;
  return node;
}

template <class _Tp>
_Tp ast_cast (AST *item)
{
  if (item && static_cast<_Tp> (0)->__node_kind == item->kind)
    return static_cast<_Tp> (item);

  return 0;
}

template <class _Tp>
_Tp ast_kind ()
{
  return _Tp::__node_kind;
}


} // namespace rpp

#endif // AST_H

// kate: space-indent on; indent-width 2; replace-tabs on;
