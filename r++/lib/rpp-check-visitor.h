/*
    Copyright (C) 2007 Frerich Raabe <raabe@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef RPP_CHECK_VISITOR_H
#define RPP_CHECK_VISITOR_H

#include "rpp-ast-visitor.h"

#include <QtCore/QList>
#include <QtCore/QString>

namespace rpp {

class Parser;

class CheckVisitor : public ASTVisitor
{
public:
  struct Result {
    QString filename;
    int line;
    QString message;
  };

  CheckVisitor();

  void setParser(const Parser *parser);

  QList<Result> results() const { return _M_results; }

protected:
  void addResult(std::size_t token_index, const QString &message);
  const Parser *parser() const { return _M_parser; }

private:
  void positionAt(std::size_t token_index, int *line, QString *filename);

  Parser *_M_parser;
  QList<Result> _M_results;
};

} // namespace rpp

#endif // RPP_CHECK_VISITOR_H

// kate: space-indent on; indent-width 2; replace-tabs on;
