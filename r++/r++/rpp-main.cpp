/*
    Copyright (C) 2002-2007 Roberto Raggi <roberto@kdevelop.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QLibrary>
#include <QtCore/QTextStream>
#include <QtCore/QtDebug>

#include "rpp-ast.h"
#include "rpp-check-visitor.h"
#include "rpp-tokens.h"
#include "rpp-lexer.h"
#include "rpp-parser.h"
#include "rpp-control.h"

#include <dlfcn.h>

using namespace rpp;

typedef rpp::CheckVisitor *(*VisitorFactoryFn)();

static const char factoryFnName[] = "create_instance";

void print_results(const QList<CheckVisitor::Result> &results)
{
  foreach (CheckVisitor::Result r, results)
    {
      printf("%s:%d: %s\n", qPrintable (r.filename), r.line, qPrintable (r.message));
    }
}

bool check_file (const QString &fileName, bool, const QList<VisitorFactoryFn> &factories)
{
  QFile file (fileName);
  if (! file.open (QFile::ReadOnly))
    return false;

  QByteArray contents = file.readAll ();
  file.close();

  Control control;
  Parser p (&control);
  pool __pool;

  TranslationUnitAST *ast = p.parse (contents, contents.size(), &__pool);

  if (! control.errorMessages ().isEmpty ())
    {
      fprintf (stderr, "*** Found %d error(s)\n", control.errorMessages ().size ());

      foreach (Control::ErrorMessage err, control.errorMessages ())
        fprintf (stderr, "\t%s:%d: %s\n", qPrintable (err.fileName ()), err.line (), qPrintable (err.message ()));
    }
  else
    {
      foreach (VisitorFactoryFn fn, factories)
        {
          CheckVisitor *v = fn ();
          v->setParser (&p);
          ast->accept (v, 0);
          print_results (v->results ());
          delete v;
        }
    }
  return true;
}


QList<VisitorFactoryFn> load_visitor_factories()
{
  QList<VisitorFactoryFn> functions;
  foreach (QString n, QDir::current ().entryList (QStringList () << "libr++check_*.so"))
    {
      void *lib = ::dlopen (QDir::current ().absoluteFilePath (n).toLatin1(), RTLD_LAZY);
      if (! lib )
        {
          fprintf (stderr, "Failed to load plugin %s: %s\n", qPrintable (QDir::current ().absoluteFilePath (n)), ::dlerror ());
          continue;
        }

      VisitorFactoryFn fn = (VisitorFactoryFn)::dlsym (lib, factoryFnName);
      if (! fn)
        {
          fprintf (stderr, "Failed to resolve factory function %s in file %s\n", factoryFnName, qPrintable (n));
          continue;
        }

      printf ("Loaded plugin %s\n", qPrintable (n));
      functions.append( fn );
    }
  return functions;
}

int main (int, char *argv[])
{
  const char *filename = 0;
  bool dump = false;

  QList<VisitorFactoryFn> factories = load_visitor_factories();

  while (const char *arg = *++argv)
    {
      if (! strcmp (arg, "-dump"))
        dump = true;

      else
        {
          filename = arg;
          break;
        }
    }

  if (! filename)
    fprintf (stderr, "r++: no input file\n");

  else if (check_file (filename, dump, factories))
    return EXIT_SUCCESS;

  return EXIT_FAILURE;
}

// kate: space-indent on; indent-width 2; replace-tabs on;
