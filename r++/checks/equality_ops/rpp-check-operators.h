#ifndef RPP_CHECK_OPERATORS_H
#define RPP_CHECK_OPERATORS_H

#include "rpp-check-visitor.h"

#include <QtCore/QList>

namespace rpp {

class TokenStream;
class Parser;

class CheckOperators: public CheckVisitor
{
public:
  struct Info
  {
    Info(ClassSpecifierAST *class_specifier = 0):
      class_specifier(class_specifier),
      has_operator_eq(0),
      has_operator_not_eq(0) {}
    
    ClassSpecifierAST *class_specifier;
    uint has_operator_eq: 1;
    uint has_operator_not_eq: 1;
  };
  
protected:  
  virtual bool visit (ClassSpecifierAST *, SemanticEnvironment *);
  virtual void endVisit (ClassSpecifierAST *, SemanticEnvironment *);

  virtual bool visit (FunctionDefinitionAST *, SemanticEnvironment *);
  virtual bool visit (InitDeclaratorAST *, SemanticEnvironment *);

private:
  QList<Info> _M_info;
};
  
} // namespace rpp

#endif // RPP_CHECK_OPERATORS_H
