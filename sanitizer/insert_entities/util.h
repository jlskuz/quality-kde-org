/**
 * util.h - (C) 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <vector>

std::vector<std::string> split( const std::string &s, std::string::value_type sep );
std::string stripWhitespace( const std::string &s );
std::string simplifyWhitespace( const std::string &s );
std::vector<std::string> filesInDirectory( const std::string &dirName );
bool fileIsDirectory( const std::string &fileName );

#endif // UTIL_H

