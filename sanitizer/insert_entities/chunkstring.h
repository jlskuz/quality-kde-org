/**
 * chunkstring.h - (C) 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#ifndef CHUNKSTRING_H
#define CHUNKSTRING_H

#include <string>
#include <vector>

class ChunkString
{
	public:
		ChunkString( const std::string &s, std::string::value_type sep, std::string::value_type newSep = ' ' );

		std::string streamlineString() const { return m_string; }
		std::string &streamlineString() { return m_string; }
		std::string chunkString() const;

		std::string::size_type find( const std::string &needle, std::string::size_type pos = 0 );
		void replace( std::string::size_type pos, const std::string &needle, const std::string &replacement );
		void replaceAll( const std::string &needle, const std::string &replacement );

	private:
		typedef std::vector<std::string::size_type> Separators;

		Separators m_separators;
		std::string m_string;
		std::string::value_type m_origSep;
		std::string::value_type m_newSep;
};

#endif // CHUNKSTRING_H
