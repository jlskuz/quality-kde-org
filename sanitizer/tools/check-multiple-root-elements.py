#!/usr/bin/python
import sys

hits=0
startOfDocument=1
depth=0

try:
	for line in open(sys.argv[1], 'r').readlines():
		pos = line.find("<")
		while pos != -1:

			# "</..." means it's a closing tag
			if line[pos + 1] == '/':
				depth -= 1
			else:
				if depth == 0 and startOfDocument == 0:
					print "Found additional root element at line <span class=\"lineno\">%s</span>; " % line
					print line.rstrip()
					print " " * pos + "^"
					hits += 1

				# Opening tags always look like "<...>", but not "<.../>"
				closingBracePos = line.find(">", pos + 1)
				if line[closingBracePos - 1] != '/' and line[closingBracePos - 2] != '/':
					depth += 1

				startOfDocument=0

			pos = line.find("<", pos + 1)

except IOError, e:
	print e

sys.exit(hits)

