<?php
$breadcrumbs = array();
$breadcrumbs[] = array("name" => "Home", "url" => "/index.php");
$breadcrumbs[] = array("name" => "Just-in-Time DocBook Sanitizer");
include 'header.inc';
include 'about.inc';
?>
<p>There are three ways you can submit markup to be checked:</p>
<ul type="square">
<li>You can <a href="#by-url">specify an URL</a> referencing a document to be
checked. This is useful if the document is already available on the web (for
instance, through KDE's <a href="http://websvn.kde.org">WebSVN</a>
interface).</li>
<li>You can also <a href="#upload">upload a file</a> which you have on your
computer.</li>
<li>Finally, markup can be <a href="#direct">specified directly</a> online,
usually by copy &amp; paste from some other location.</li>
</ul>
<h2><a name="by-url">Validate by URL</a></h2>
<form method="get" action="/cgi-bin/check.py">
        <label title="Address of document to check" for="uri">Address:&nbsp;<input class="uri" name="uri" size="40"/></label>
        <label title="Submit address"><input type="submit" value="Check"/></label>
</form>
<p>Enter the <abbr title="Uniform Resource Locator">URL</abbr> which references
the document you would like to check.</p>

<h2><a name="upload">Validate by File Upload</a></h2>
<form method="post" enctype="multipart/form-data" action="/cgi-bin/check.py">
        <label title="Choose a local file to upload and check" for="uploaded_file">Local file:&nbsp;<input type="file" class="uploaded_file" name="uploaded_file" size="30"/></label>
        <label title="Submit file"><input type="submit" value="Check"/></label>
</form>
<p>Select a file on your local harddisk which shall be uploaded and
checked. Note that in very rare cases (in particular, when you're using
Internet Explorer on some versions of Windows XP Service Pack 2) this doesn't
work. Just fall back to one of the other ways of submitting markup in that
case.</p>

<h2><a name="direct">Validate by Input</a></h2>
<form method="post" enctype="multipart/form-data" action="/cgi-bin/check.py">
        <label title="Paste a complete DocBook document here" for="markup"><textarea cols="70" rows="12" name="markup" class="markup" style="height: 300px"></textarea></label><br/>
        <label title="Submit markup"><input type="submit" value="Check"/></label>
</form>
<p>You can paste some DocBook markup here to have it checked. Note that
even though checking little fragments of markup is perfectly possible, the
test which checks for whether the input is valid XML might fail.</p>
<?php
        include 'footer.inc';
?>
