#!/bin/sh

# This is a script which can be run via cron to execute a Nightly build of all
# KDE modules for the KDE dashboards at my.cdash.org.
#
# To use it, you have to adjust it slightly, it should be basically the 
# following five variables:

CTEST=/opt/cmake-2.8.1-Linux-i386/bin/ctest
INSTALL_ROOT=/home/alex/Dashboards/installs/2.8.1
SCRIPT_DIR=/home/alex/src/kde4-svn/KDE\ dir/kdesdk/cmake/nightly-support/
SUFFIX=gcc-4.3.2-anonsvn

# set CMAKE_PREFIX_PATH so cmake finds everything: Qt, shared-mime-info, redland, raptor, rasqal:
CMAKE_PREFIX_PATH=/opt/kde-qt:/opt/shared-mime-info:/opt/phonon/:/opt/rdf:$(INSTALL_ROOT)/kdesupport:$(INSTALL_ROOT)/kdelibs:$(INSTALL_ROOT)/kdepimlibs:$(INSTALL_ROOT)/kdebase:/opt/gpgme:/opt/ruby-1.9.1:/opt/dbusmenuqt:/opt/docbook


all: kdesupport kdelibs kdepimlibs kdebase-workspace kdebase-apps kdebase-runtime kdeaccessibility kdeadmin kdeartwork kdebindings kdeedu kdeexamples kdegames kdegraphics kdemultimedia kdenetwork kdepim plasmaaddons kdesdk kdetoys kdeutils kdewebdev 

kdesupport:
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -VV -S $(SCRIPT_DIR)/kdesupport/KDESupportNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX),CMAKE_INSTALL_PREFIX=$(INSTALL_ROOT)/kdesupport,DO_INSTALL=TRUE ; true

kdelibs: kdesupport
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -VV -S $(SCRIPT_DIR)/KDE/KDELibsNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX),CMAKE_INSTALL_PREFIX=$(INSTALL_ROOT)/kdelibs,KDE_CTEST_PARALLEL_LEVEL=2,DO_INSTALL=TRUE ; true

kdepimlibs: kdelibs
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEPIMLibsNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX),CMAKE_INSTALL_PREFIX=$(INSTALL_ROOT)/kdepimlibs,DO_INSTALL=TRUE ; true

kdebase-workspace: kdelibs kdesupport kdepimlibs
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEBaseWorkspaceNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX),CMAKE_INSTALL_PREFIX=$(INSTALL_ROOT)/kdebase,DO_INSTALL=TRUE ; true

basemodules: kdelibs kdesupport kdepimlibs kdebase-workspace

kdeaccessibility: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEAccessibilityNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdeadmin: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEAdminNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdeartwork: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEArtworkNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdebase-apps: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEBaseAppsNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdebase-runtime: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEBaseRuntimeNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdebindings: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEBindingsNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdeedu: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEEduNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdeexamples: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEExamplesNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdegames: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEGamesNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdegraphics: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEGraphicsNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdemultimedia: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEMultimediaNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdenetwork: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDENetworkNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX),KDE_CTEST_CACHE_VARIABLES=BUILD_kopete,BUILD_kopete=FALSE ; true

kdepim: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEPIMNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

plasmaaddons: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEPlasmaAddonsNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdesdk: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDESDKNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdetoys: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEToysNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdeutils: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEUtilsNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true

kdewebdev: basemodules
	CMAKE_PREFIX_PATH=$(CMAKE_PREFIX_PATH) $(CTEST) -V -S $(SCRIPT_DIR)/KDE/KDEWebDevNightly.cmake,KDE_CTEST_BUILD_SUFFIX=$(SUFFIX) ; true


