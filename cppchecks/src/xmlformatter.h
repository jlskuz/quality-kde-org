#ifndef XMLFORMATTER_H
#define XMLFORMATTER_H

#include "outputformatter.h"

class QXmlStreamWriter;
class QByteArray;

class XmlFormatter : public OutputFormatter
{
  public:
    XmlFormatter();

    ~XmlFormatter();

    virtual void printFooter() const;

    virtual void printHeader() const;

    virtual void printListFooter() const;

    virtual void printListHeader() const;

    virtual void printPlugin(QString const &name, QString const &version, QString const &desc) const;

    virtual void printResults(IssueList const &issues) const;

  private:
    QByteArray       *mResult;
    QXmlStreamWriter *mWriter;
};

#endif // XMLFORMATTER_H
