#ifndef ISSUE_H
#define ISSUE_H

#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>

#include "location.h"

class Issue
{
  public:
    void addIssueLine(QString const &line);
    QString description() const;
    QStringList lines() const;
    Location location() const;
    QString scope() const;
    void setDescription(QString const &description);
    void setLocation(Location const &location);
    void setScope(QString const &scope);

  private:
    QString     mDescription; // Description of the issue.
    QStringList mLines;       // Lines describing this specific issue.
    Location    mLocation;    // The location of the issue.
    QString     mScope;       // Scope of the issue (e.g. class, struct, global).
};

typedef QList<Issue> IssueList;
typedef QMap<QString, IssueList> IssueMap; // Issues ordered by description
typedef QMapIterator<QString, IssueList> IssueMapIterator;

enum Field
{
  Description,
  FileName,
  Scope
};

IssueMap orderIssues(IssueList const &issues, Field field);

#endif // ISSUE_H
