#include "textformatter.h"

#include <iomanip>
#include <iostream>

using namespace std;

void TextFormatter::printFooter() const
{ }

void TextFormatter::printHeader() const
{ }

void TextFormatter::printListFooter() const
{ }

void TextFormatter::printListHeader() const
{
  // ~center for a width of 80 chars.
  cout << setw(40 + 12) << "== Available plugins ==" << endl;
}

void TextFormatter::printPlugin(QString const &name, QString const &version, QString const &desc) const
{
  Q_UNUSED(version);
  cout << setw(30) << qPrintable(name) << ": " << qPrintable(desc) << endl;
}

void TextFormatter::printResults(IssueList const &issues)  const
{
  if (issues.size() == 0)
    return;

  cout << "== CppAnalyzer: " << qPrintable(mAnalyzerName) << " ==" << endl;

  IssueMap issuesPerFile = orderIssues(issues, FileName);
  IssueMapIterator i(issuesPerFile);
  while (i.hasNext()) {
    i.next();
    cout << "File: " << qPrintable(i.key()) << ": " << endl;

    IssueMap issuesPerDescription = orderIssues(i.value(), Description);
    IssueMapIterator j(issuesPerDescription);
    while (j.hasNext()) {
      j.next();
      cout << " * " << qPrintable(j.key()) << endl;
      IssueMap issuesPerScope = orderIssues(j.value(), Scope);
      IssueMapIterator k(issuesPerScope);
      while (k.hasNext()) {
        k.next();
        cout << "   - " << qPrintable(k.key()) << endl;
        foreach (Issue const &issue, k.value()) {
          foreach (QString const &line, issue.lines())
            cout << "     " << qPrintable(line) << endl;
        }
      }
    }
  }

  cout << endl;
  printDescription();
  cout << endl;
}

// Formats the description in lines of max 80 characters.
void TextFormatter::printDescription() const
{
  QStringList lines = mAnalyzerDescription.split('\n');

  foreach (QString const &line, lines) {
      QString lineToPrint;
      QStringList words = line.split(' ');
      foreach (QString const &word, words) {
        if (lineToPrint.size() + word.size() + 1 < 80) {
          if (lineToPrint.size() > 0)
            lineToPrint += ' ';
          lineToPrint += word;
        } else {
          cout << qPrintable(lineToPrint) << endl;
          lineToPrint.clear();
          if (line.startsWith('-'))
            lineToPrint += "  ";
          lineToPrint += word;
        }
      }
      if (!lineToPrint.isEmpty())
        cout << qPrintable(lineToPrint) << endl;
  }
}
