#ifndef VIRTUALSCHECKVISITOR_H
#define VIRTUALSCHECKVISITOR_H

#include <pluginbase/checkvisitor.h>

class VirtualMismatchCheckVisitor : public CheckVisitor
{
  public: // Methods
    VirtualMismatchCheckVisitor(EFES::ExtractionUnitPtr const &eup);

  protected: //Methods
    virtual Visit check(EFES::ASTNode const &node);

  private: // Methods
    void processClass(EFES::TS_classSpec const &classSpec); //Processes a class decl found by visitor
};

#endif // VIRTUALSCHECKVISITOR_H
