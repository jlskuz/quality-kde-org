set(virtualmismatchcheck_SRCS
  virtualmismatchcheckvisitor.cpp
  virtualmismatchcheck.cpp
)

set(virtualmismatchcheck_MOC_HDRS
  virtualmismatchcheck.h
)

add_definitions(${QT_DEFINITIONS})
add_definitions(-DQT_PLUGIN)
add_definitions(-DQT_SHARED)

qt4_wrap_cpp(virtualmismatchcheck_MOC_SRCS ${virtualmismatchcheck_MOC_HDRS})

add_library(virtualmismatchcheck SHARED
            ${virtualmismatchcheck_SRCS}
            ${virtualmismatchcheck_MOC_SRCS}
)

target_link_libraries(virtualmismatchcheck
  pluginbase
  ${QT_QTCORE_LIBRARY}
  ${SolidFX_FXCXXAPI_LIBRARY}
)
