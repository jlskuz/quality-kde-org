#ifndef OPERATORSCHECK_H
#define OPERATORSCHECK_H

#include <pluginbase/analyzerplugin.h>

class OperatorsCheck : public QObject, public AnalyzerInterface
{
  Q_OBJECT
  Q_INTERFACES(AnalyzerInterface)

  public:
    OperatorsCheck();

    virtual QString description() const;

    virtual QString name() const;

    virtual void run(EFES::ExtractionUnitPtr const &eup);

    virtual QString shortDescription() const;

    virtual QString version() const;
};

#endif // OPERATORSCHECK_H
