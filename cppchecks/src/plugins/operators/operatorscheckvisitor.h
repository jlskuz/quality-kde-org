#ifndef OPERATORSCHECKVISITOR_H
#define OPERATORSCHECKVISITOR_H

#include <pluginbase/checkvisitor.h>

namespace EFES {

  namespace OverloadUtils {
    class OVFunctionsSig;
  }
}

class OperatorsCheckVisitor : public CheckVisitor
{
  public: // Methods
    OperatorsCheckVisitor(EFES::ExtractionUnitPtr const &eup);

  protected: //Methods
    virtual Visit check(EFES::ASTNode const &node);

  private: // Methods
    void processClass(EFES::TS_classSpec const &classSpec); //Processes a class decl found by visitor

    void processOperatorAssign(EFES::TS_classSpec const &classSpec,
                               EFES::OverloadUtils::OVFunctionsSig const &function);

    void processComparisonOperator(QString const &operatorName,
                                   EFES::TS_classSpec const &classSpec,
                                   EFES::OverloadUtils::OVFunctionsSig const &function);
};

#endif // OPERATORSCHECKVISITOR_H
