#include "operatorscheckvisitor.h"

#include <QtCore/QString>

#include <added/OverloadUtils.h>
#include <added/TypeFactory.h>

using namespace EFES;
using namespace OverloadUtils;

OperatorsCheckVisitor::OperatorsCheckVisitor(EFES::ExtractionUnitPtr const &eup)
    : CheckVisitor(eup)
{ }

ASTVisitor::Visit OperatorsCheckVisitor::check(ASTNode const &n)
{
  if (inUserSpace(n) && n.nodeType() == TYPE_TS_classSpec) {
    TS_classSpec const *cs = n.asTypeSpecifier()->asTS_classSpec();
    processClass(*cs);
  }

  return VISIT_CHILDREN_AND_POST;
}

void OperatorsCheckVisitor::processClass(TS_classSpec const &classSpec)
{
  if (classSpec.keyword != TI_CLASS)
    return;

  CompoundOverloads overloads;
  overloads.initialize(classSpec.ctype, false); // No need to look up the hierachy

  for (CompoundOverloads::NameToOVFunctions::const_iterator it = overloads.methods.begin();
       it != overloads.methods.end(); ++it) {
    QString methodName((*it).first.c_str());
    if (!methodName.startsWith("operator"))
      continue; // Only check operators

    CompoundOverloads::OVFunctionsName const *mv = it->second;
    foreach (OVFunctionsSig const *overloadedOperator, *mv) {
      if (methodName == "operator=")
        processOperatorAssign(classSpec, *overloadedOperator);
      // Check the comparison operators.
      if (methodName == "operator=="
          || methodName == "operator!="
          || methodName == "operator>"
          || methodName == "operator<"
          || methodName == "operator>="
          || methodName == "operator<=")
        processComparisonOperator(methodName, classSpec, *overloadedOperator);
    }
  }
}

void OperatorsCheckVisitor::processOperatorAssign(TS_classSpec const &classSpec,
                                                  OVFunctionsSig const &signature)
{
  foreach (OVFunction const * function, signature) {
    OVFunction const * function = signature.front();
    Variable const * variable = function->var;

    //REMARK: const_cast ok, the TypeFactory must have non-const args but won't change them
    FunctionType* functionType = const_cast<FunctionType*>(function->type);
    if (functionType->params.size() != 2) { //REMARK: operator= has _two_ parameters in the type-system
      // Not canonical form create issue.
      Issue issue;
      issue.setDescription("operator= has not the canonical form.");
      issue.setLocation(location(*variable));
      issue.setScope(classSpec.ctype->toString().c_str());

      issue.addIssueLine("Wrong number of parameters");
      mIssues.append(issue);
    }

    //make type of what the return of op= should be, and get what it really is
    BasicTypeFactory typeFactory;
    Type const *expectedReturnType = typeFactory.makeTypeOf_receiver(functionType->getClassOfMember(), CV_NONE, 0);
    Type const *actualReturnType = functionType->retType;
    if (!actualReturnType || !actualReturnType->equals(expectedReturnType)) { //the two return types should be equal
       // Return type different, create issue.
      Issue issue;
      issue.setDescription("operator= has not the canonical form.");
      issue.setLocation(location(*variable));
      issue.setScope(classSpec.ctype->toString().c_str());

      issue.addIssueLine("Return types differ");
      issue.addIssueLine(QString("Expected: ") + expectedReturnType->toString().c_str());
      issue.addIssueLine(QString("Got: ") + actualReturnType->toString().c_str());
      mIssues.append(issue);
    }

    //same type comparison for the actual (only) arg of the function
    //(note that the arg is 'const X&' where 'X' is the class' type
    Type const *expectedArgumentType = typeFactory.makeTypeOf_receiver(functionType->getClassOfMember(), CV_CONST, 0);
    Type const *actualArgumentType = (*(++functionType->params.begin()))->type;
    if (!actualArgumentType->equals(expectedArgumentType)) {
      Issue issue;
      issue.setDescription("operator= has not the canonical form.");
      issue.setLocation(location(*variable));
      issue.setScope(classSpec.ctype->toString().c_str());

      issue.addIssueLine("Argument types differ");
      issue.addIssueLine(QString("Expected: ") + expectedArgumentType->toString().c_str());
      issue.addIssueLine(QString("Got: ") + actualArgumentType->toString().c_str());
      mIssues.append(issue);
    }
  }
}

void OperatorsCheckVisitor::processComparisonOperator(QString const &operatorName,
                                                      EFES::TS_classSpec const &classSpec,
                                                      EFES::OverloadUtils::OVFunctionsSig const &signature)
{
  foreach (OVFunction const * function, signature) {
    OVFunction const * function = signature.front();
    Variable const * variable = function->var;
    FunctionType const *functionType = function->type;

    // Check the return type.
    if (!functionType->retType->asSimpleType()
        || functionType->retType->asSimpleType()->toString() != "bool") {
      Issue issue;
      issue.setDescription("Comparison operator with non bool return value.");
      issue.setLocation(location(*variable));
      issue.setScope(classSpec.ctype->toString().c_str());
      issue.addIssueLine(operatorName + " returns: " + functionType->retType->toString().c_str());
      mIssues.append(issue);
    }

    // Check the constness of the receiving object.
    if (!(functionType->getReceiverCV() & EFES::CV_CONST)) {
      Issue issue;
      issue.setDescription("Non const comparison operator function.");
      issue.setLocation(location(*variable));
      issue.setScope(classSpec.ctype->toString().c_str());
      issue.addIssueLine(operatorName);
      mIssues.append(issue);
    }

    //REMARK: comparison operators have _two_ parameters in the type-system
    if (functionType->params.size() != 2) {
      // Not canonical form create issue.
      Issue issue;
      issue.setDescription("Comparison operator with more than one argument.");
      issue.setLocation(location(*variable));
      issue.setScope(classSpec.ctype->toString().c_str());
      issue.addIssueLine(operatorName + ": "
                         + QString::number(functionType->params.size() - 1)
                         + " arguments");
      mIssues.append(issue);
    }

    Type const *argumentType = (*(++functionType->params.begin()))->type;
    if (argumentType->asReferenceType()) {
      ReferenceType const *refType = argumentType->asReferenceType();

      if (!(refType->atType->getCVFlags() & EFES::CV_CONST)) {
        Issue issue;
        issue.setDescription("Non const comparison operator argument.");
        issue.setLocation(location(*variable));
        issue.setScope(classSpec.ctype->toString().c_str());
        issue.addIssueLine(operatorName + ": " + argumentType->toString().c_str());
        mIssues.append(issue);
      }
    } else {
      Issue issue;
      issue.setDescription("Non reference comparison operator argument.");
      issue.setLocation(location(*variable));
      issue.setScope(classSpec.ctype->toString().c_str());
      issue.addIssueLine(operatorName + ": " + argumentType->toString().c_str());
      mIssues.append(issue);
    }
  }
}

