#include "virtualscheckvisitor.h"

#include <added/FileTable.h>
#include <EfesAPI/FactDBEngine.h>

#include "classhierarchychecker.h"

using namespace EFES;

VirtualsCheckVisitor::VirtualsCheckVisitor(ExtractionUnitPtr const &p)
  : CheckVisitor(p)
{}

//Visit an AST tree. Select the usages of symbols, and check if they're defined in user-headers
ASTVisitor::Visit VirtualsCheckVisitor::check(ASTNode const &n)
{
  if (inUserSpace(n) && n.nodeType() == TYPE_TS_classSpec) {
    TS_classSpec const *cs = n.asTypeSpecifier()->asTS_classSpec();
    processClass(cs);
  }

  return VISIT_CHILDREN_AND_POST;
}

void VirtualsCheckVisitor::processClass(TS_classSpec const *classSpec)
{
  ClassHierarchyChecker checker;
  checker.initialize(location(*classSpec), classSpec);
  checker.analyze(); //do whatever analyses we want
  mIssues << checker.issues();
}
