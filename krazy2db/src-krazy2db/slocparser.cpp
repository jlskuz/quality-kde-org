#include "slocparser.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QMap>
#include <QtCore/QRegExp>
#include <QtCore/QStringList>

/// SLOCForDirData

class SLOCForDirData : public QSharedData
{
public: /// Members
  QString mDir;
  QMap<QString, int> mCountPerLanguage;
};

/// SLOCForDir

SLOCForDir::SLOCForDir( const QString &dir )
  : d( new SLOCForDirData )
{
  d->mDir = dir;
}

SLOCForDir::SLOCForDir( const SLOCForDir &other )
  : d( new SLOCForDirData )
{
  d = other.d;
}

SLOCForDir::~SLOCForDir()
{ }

SLOCForDir &SLOCForDir::operator=( const SLOCForDir &other )
{
  if ( this != &other )
    d = other.d;

  return *this;
}

void SLOCForDir::put( const QString &language, int count )
{
  d->mCountPerLanguage[language] = count;
}

QString SLOCForDir::dir() const
{
  return d->mDir;
}

QStringList SLOCForDir::languages() const
{
  return d->mCountPerLanguage.keys();
}

int SLOCForDir::count() const
{
  int total = 0;
  const QList<int> slocs = d->mCountPerLanguage.values();
  foreach ( int sloc, slocs )
    total += sloc;

  return total;
}

int SLOCForDir::count( const QString &language ) const
{
  return d->mCountPerLanguage.value( language );
}

/// SlOCParserPrivate

class SLOCParserPrivate
{
public: /// Enum
  enum State {
    BeforeSeperator, // Seperator line looks something like: SLOC    Directory       SLOC-by-Language (Sorted)
    AfterSeperator,
    Error,
    Success
  };

public: /// Members
  State mCurrentState;
  QString mErrorMessage;
  QList<SLOCForDir> mResults;

private: /// Methods
  bool isSeperator( const QString &line );
  bool processSLOCLine( const QString& line );

public: /// Methods
  bool processLine( const QString &line );
  void reset();
};

bool SLOCParserPrivate::processLine( const QString &line )
{
  switch ( mCurrentState ) {
  case BeforeSeperator:
    if ( isSeperator( line ) )
      mCurrentState = AfterSeperator;
    return false;
  case AfterSeperator:
    if ( line.isEmpty() ) {
      mCurrentState = Success;
      return true;
    } else
      return processSLOCLine( line );
  default:
    Q_ASSERT( false ); // Should not happen
    return true;
  }
}

void SLOCParserPrivate::reset()
{
  mCurrentState = BeforeSeperator;
  mErrorMessage.clear();
}

static QRegExp sSeperatorRegeExp = QRegExp( "^SLOC\\s+Directory\\s+SLOC-by-Language" );
bool SLOCParserPrivate::isSeperator( const QString &line )
{
  return sSeperatorRegeExp.indexIn( line ) != -1;
}

bool SLOCParserPrivate::processSLOCLine( const QString &line )
{
  Q_ASSERT( !line.isEmpty() );

  const QStringList splittedLine = line.simplified().split( ' ' );

  SLOCForDir sloc( "" );
  QStringList countsPerLanguage;

  if ( splittedLine.size() == 3 ) {
    bool ok = false;
    const int count = splittedLine.at( 0 ).toInt( &ok );
    Q_ASSERT( ok );

    if ( count == 0 ) // Skip directories that have no lines.
      return false;

    sloc = SLOCForDir( splittedLine.at( 1 ) );
    countsPerLanguage = splittedLine.at( 2 ).split( ',' );
  } else { // Results of this line belong to the previous line
    sloc = mResults.takeLast();
    countsPerLanguage = splittedLine.at( 0 ).split( ',' );
  }

  foreach ( const QString countPerLanguage, countsPerLanguage ) {
    if ( countPerLanguage == "(none)" || countPerLanguage.isEmpty() )
      continue; // Ignore languages for which no sloc is reported.

    const QStringList langCount = countPerLanguage.split( '=' );
    Q_ASSERT( langCount.size() == 2 );
    bool ok = false;
    const int count = langCount.at( 1).toInt( &ok );
    Q_ASSERT( ok );
    sloc.put( langCount.at( 0 ), count );
  }

  // Q_ASSERT( sloc.count() == count ); // Can't do this is the sloc might be specified on more lines.
  Q_ASSERT( !sloc.dir().isEmpty() ); // Make sure that it got initialized correct.
  mResults.append( sloc );
  return false; // we cannot know if we're finished yet.
}

/// SLOCParser

SLOCParser::SLOCParser()
  : d_ptr( new SLOCParserPrivate )
{ }

SLOCParser::~SLOCParser()
{
  delete d_ptr;
}

bool SLOCParser::parse( QFile *slocfile )
{
  Q_ASSERT( slocfile->exists() );
  Q_ASSERT( slocfile->isOpen() );
  Q_D( SLOCParser );

  d->reset();
  slocfile->seek( 0 );

  bool ready = false;
  while ( !slocfile->atEnd() && !ready ) {
    const QString line = slocfile->readLine().trimmed();
    ready = d->processLine( line );
    if ( ready && d->mCurrentState == SLOCParserPrivate::BeforeSeperator ) {
      d->mCurrentState = SLOCParserPrivate::Error;
      d->mErrorMessage = QLatin1String( "No seperator line found." );
    }
  }

  return d->mCurrentState == SLOCParserPrivate::Success;
}

QString SLOCParser::errorMessage() const
{
  Q_D( const SLOCParser );

  // Just to make sure that we did everything correct.
  if ( d->mCurrentState != SLOCParserPrivate::Error ) {
    Q_ASSERT( d->mErrorMessage.isEmpty() );
  } else {
    Q_ASSERT( !d->mErrorMessage.isEmpty() );
  }

  return d->mErrorMessage;
}

QList<SLOCForDir> SLOCParser::result() const
{
  Q_D( const SLOCParser );
  return d->mResults;
}
