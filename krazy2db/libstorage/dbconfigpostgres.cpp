/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) <year>  <name of author>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "dbconfigpostgres.h"

#include <QtSql/QSqlDriver>
#include <QtSql/QSqlQuery>

DbConfigPostgres::DbConfigPostgres( QSettings &settings )
{
  // read settings for current driver
  settings.beginGroup( QLatin1String( "QPSQL" ) );
  mDatabaseName = settings.value( QLatin1String( "Name" ) ).toString();
  mHostName = settings.value( QLatin1String( "Host" ) ).toString();
  mUserName = settings.value( QLatin1String( "User" ) ).toString();
  mPassword = settings.value( QLatin1String( "Password" ) ).toString();
  mConnectionOptions = settings.value( QLatin1String( "Options" ) ).toString();
  settings.endGroup();
}


void DbConfigPostgres::apply( QSqlDatabase& database ) const
{
  database = QSqlDatabase::addDatabase( QLatin1String( "QPSQL" ) );
  if ( !mDatabaseName.isEmpty() )
    database.setDatabaseName( mDatabaseName );
  if ( !mHostName.isEmpty() )
    database.setHostName( mHostName );
  if ( !mUserName.isEmpty() )
    database.setUserName( mUserName );
  if ( !mPassword.isEmpty() )
    database.setPassword( mPassword );

  database.setConnectOptions( mConnectionOptions );
  database.open();
  Q_ASSERT( database.driver()->hasFeature( QSqlDriver::LastInsertId ) );
}

bool DbConfigPostgres::createTable( QSqlDatabase& database, DbConfig::Table table ) const
{
  Q_ASSERT( database.isOpen() );
  QString tableCreateQuery;
  switch( table ) {
  case Tools:
    tableCreateQuery = "CREATE TABLE tools ("
                          "id SERIAL PRIMARY KEY, name TEXT NOT NULL,"
                          "version TEXT NOT NULL, description TEXT );";
    break;
  case Plugins:
    tableCreateQuery = "CREATE TABLE plugins ("
                          "id SERIAL PRIMARY KEY,"
                          "tool_id INTEGER REFERENCES tools (id),"
                          "name TEXT NOT NULL, version TEXT NOT NULL, description TEXT,"
                          "filetype TEXT, is_extra BOOLEAN );";
    break;
  case ComponentCheck:
    tableCreateQuery = "CREATE TABLE component_check ("
                          "id SERIAL PRIMARY KEY, component TEXT, "
                          "revision TEXT," "date TEXT );";
    break;
  case ModuleSubmodule:
    tableCreateQuery = "CREATE TABLE module_submodule ("
                         "id SERIAL PRIMARY KEY,"
                         "module TEXT, submodule TEXT);";
    break;
  case CheckedFiles:
    tableCreateQuery = "CREATE TABLE checked_files ("
                          "id SERIAL PRIMARY KEY, module_submodule_id INTEGER REFERENCES module_submodule (id), "
                          "file TEXT, type TEXT );";
    break;
  case CheckedFileResults:
    tableCreateQuery = "CREATE TABLE checked_file_results ("
                          "checked_files_id INTEGER REFERENCES checked_files (id), "
                          "component_check_id INTEGER REFERENCES component_check (id), "
                          "plugin_id INTEGER REFERENCES plugins (id), "
                          "issue_count INTEGER, "
                          "PRIMARY KEY (checked_files_id, component_check_id, plugin_id) );";
    break;
  case ModuleSubmoduleSloc:
    tableCreateQuery = "CREATE TABLE module_submodule_sloc ("
                         "component_check_id INTEGER REFERENCES component_check (id),"
                         "module_submodule_id INTEGER REFERENCES module_submodule (id),"
                         "language TEXT, sloc INTEGER);";
    break;
  default:
    Q_ASSERT( false );
  }

  QSqlQuery query( database );
  return query.exec( tableCreateQuery );
}
