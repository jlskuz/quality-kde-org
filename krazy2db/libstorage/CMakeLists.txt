set(storage_SRCS
  plugin.cpp
  tool.cpp
  dbconfig.cpp
  dbconfigsqlite.cpp
  dbconfigpostgres.cpp
  database.cpp
)

add_library(storage ${storage_SRCS})
target_link_libraries(storage
  ${QT_QTCORE_LIBRARY}
  ${QT_QTSQL_LIBRARY}
  storage
)
