#include <iostream>

#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtCore/QScopedPointer>
#include <QtCore/QStringList>

#include <libstorage/dbconfig.h>

#include "exporthelper.h"

using namespace std;

void slocPerComponent( const ExportHelper &helper )
{
  // First the header
  cout << "== Dataset: Lines of code (MLOC) per component" << endl;
  cout << "Component\tLines of code (MLOC)" << endl;

  // Then the data
  const QStringList components = helper.components();
  foreach ( const QString &component, components ) {
    const double kloc = helper.sloc( component ) / 1000000.0;
    cout << qPrintable( component ) << "\t" << kloc << endl;
  }

  cout << endl;
}

void slocPerComponentModule( const ExportHelper &helper )
{
  // First the header
  cout << "== Dataset: Lines of code (KLOC) per module" << endl;
  cout << "Module";

  const QStringList components = helper.components();
  // NOTE: If component contains colons (':') we need to do some replacement
  foreach ( const QString &component, components )
    cout << "\t" << qPrintable( component ) << ": LOC (KLOC)";

  cout << endl;

  // Then the data
  const QStringList modules = helper.modules();
  foreach ( const QString &module, modules ) {
    cout << qPrintable( module );
    foreach ( const QString &component, components ) {
      const double kloc = helper.slocPerModule( component, module ) / 1000.0;
      cout << "\t" << kloc;
    }
    cout << endl;
  }

  cout << endl;
}

void slocPerComponentModuleSubmodule( const ExportHelper &helper )
{
  // First the header
  cout << "== Dataset: Lines of code (KLOC) per module/submodule" << endl;
  cout << "Module\tSubmodule";

  const QStringList components = helper.components();
  // NOTE: If component contains colons (':') we need to do some replacement
  foreach ( const QString &component, components )
    cout << "\t" << qPrintable( component ) << ": LOC (KLOC)";

  cout << endl;

  // Then the data
  const QStringList modules = helper.modules();
  foreach ( const QString &module, modules ) {
    const QStringList submodules = helper.submodules( module );
    foreach ( const QString &submodule, submodules ) {
      cout << qPrintable( module ) << "\t" << qPrintable( submodule );
      foreach ( const QString &component, components ) {
        const double kloc = helper.slocPerSubmodule( component, module, submodule ) / 1000.0;
        cout << "\t" << kloc;
      }
      cout << endl;
    }
  }

  cout << endl;
}

void slocPerLanguagePerComponent( const ExportHelper &helper )
{
  // First the header
  cout << "Component";
  const QStringList languages = helper.languages();
  foreach ( const QString &language, languages )
    cout << "\t" << qPrintable( language ) << " (kloc)";

  cout << endl;

  // Then the data
  const QStringList components = helper.components();
  foreach ( const QString &component, components ) {
    cout << qPrintable( component );
    foreach ( const QString &language, languages ) {
      const double kloc = helper.sloc( component, language ) / 1000.0;
      cout <<  "\t" << kloc;
    }
    cout << endl;
  }

  cout << endl;
}

void slocPerLanguagePerComponentModule( const ExportHelper &helper )
{
  // First the header
  // Module | Language | kde-4.0.0 | kde-4.1.0

  cout << "== Dataset: Lines of code (KLOC) per language/module" << endl;
  cout << "Module\tLanguage";

  const QStringList components = helper.components();

  // NOTE: If component contains colons (':') we need to do some replacement
  foreach ( const QString &component, components )
    cout << '\t' << qPrintable( component );

  cout << endl;

  // Then the data
  const QStringList languages = helper.languages();
  const QStringList modules = helper.modules();
  foreach ( const QString &module, modules ) {
    foreach ( const QString &language, languages ) {
      cout << qPrintable( module ) << '\t' << qPrintable( language );
      foreach ( const QString &component, components ) {
        const double kloc = helper.slocPerModule( component, module, language ) / 1000.0;
        cout << "\t" << kloc;
      }
      cout << endl;
    }
  }

  cout << endl;
}

void issuesPerComponent( const ExportHelper &helper )
{
  // First the header
  cout << "Component\tKrazy2 issues (#1000s)" << endl;

  // Then the data
  const QStringList components = helper.components();
  foreach ( const QString &component, components ) {
    const double issues = helper.issues( component ) / 1000.0;
    cout << qPrintable( component ) << "\t" << issues << endl;
  }

  cout << endl;
}

void issuesPerComponentModule( const ExportHelper &helper )
{
  // First the header
  // Module | kde-4.0.0 | kde-4.1.0

  cout << "Module";
  const QStringList components = helper.components();
  foreach ( const QString &component, components )
    cout << '\t' << qPrintable( component ) << " (#)";
  cout << endl;

  // Then the data
  const QStringList modules = helper.modules();
  foreach ( const QString &module, modules ) {
    cout << qPrintable( module );
    foreach ( const QString &component, components ) {
      const double issues = helper.issuesPerModule( component, module );
      cout << '\t' << issues;
    }
    cout << endl;
  }

  cout << endl;
}

void issuesPerPluginComponentModule( const ExportHelper &helper )
{
  // First the header
  // Module | Plugin | kde-4.0.0 | kde-4.1.0

  cout << "Module\tPlugin";
  const QStringList components = helper.components();
  foreach ( const QString &component, components )
    cout << '\t' << qPrintable( component ) << " (#)";
  cout << endl;

  // Then the data
  const QStringList modules = helper.modules();
  foreach ( const QString &module, modules ) {
    // For now we just hardcode the toolname.
    foreach( const QString &plugin, helper.plugins( "krazy2" ) ) {
      cout << qPrintable( module ) << '\t' << qPrintable( plugin );
      foreach ( const QString &component, components ) {
        const double issues = helper.issuesPerModule( component, module, plugin );
        cout << '\t' << issues;
      }
      cout << endl;
    }
  }

  cout << endl;
}

void slocIssuesRatioComponent( const ExportHelper &helper )
{
  cout << "Component\tLines of code (MLOC)\tKrazy2 issues (#1000s)\tRatio (#issues/kloc)" << endl;

  const QStringList components = helper.components();
  foreach ( const QString &component, components ) {
    const double loc = helper.sloc( component );// / 1000000.0;
    const double issues = helper.issues( component );// / 1000.0;
    cout << qPrintable( component ) << '\t' << loc/1000000.0 << '\t' << issues/1000 << '\t' << issues/(loc/1000) << endl;
  }

  cout << endl;
}

void slocIssuesRatioComponentModule( const ExportHelper &helper )
{
  // First the header
  cout << "Module";

  const QStringList components = helper.components();
  // NOTE: If component contains colons (':') we need to do some replacement
  foreach ( const QString &component, components ) {
    cout << "\t" << qPrintable( component ) << ": LOC (KLOC)";
    cout << "\t" << qPrintable( component ) << ": Krazy2 issues (#)";
    cout << '\t' << qPrintable( component ) << ": Ratio (#issues/kloc)";
  }

  cout << endl;

  // Then the data
  const QStringList modules = helper.modules();
  foreach ( const QString &module, modules ) {
    cout << qPrintable( module );
    foreach ( const QString &component, components ) {
      const double loc = helper.slocPerModule( component, module );
      const double issues = helper.issuesPerModule( component, module );
      cout << '\t' << loc/1000 << '\t' << issues << '\t' << issues/(loc/1000);
    }
    cout << endl;
  }

  cout << endl;
}

int main( int argc, char *argv[] )
{
  QScopedPointer<DbConfig> dbConfig;
  if( argc == 3) {
    QFileInfo settingsFile( argv[2] );
    if ( !settingsFile.exists() ) {
      cerr << "Settings file " << argv[2] << " does not exists." << endl;
      return 5;
    }
    if ( !settingsFile.isReadable() ) {
      cerr << "Can not open settings file " << argv[2] << " for reading." << endl;
      return 5;
    }

    QSettings dbSettings( settingsFile.absoluteFilePath(), QSettings::IniFormat );
    if ( dbSettings.status() != QSettings::NoError ) {
      cerr << "Could not parse settings file " << argv[2] << "." << endl;
      return 6;
    }
    dbConfig.reset( DbConfig::configuredDatabase( dbSettings ) );
  } else {
    // Create a default configuration, by passing an invalid QSetting
    QSettings useDefaultSettings;
    dbConfig.reset( DbConfig::configuredDatabase( useDefaultSettings ) );
  }

  QSqlDatabase db;
  dbConfig->apply( db );

  ExportHelper helper( &db );

//  slocPerComponent( helper );
//  slocPerComponentModule( helper );
//  slocPerComponentModuleSubmodule( helper );

//  slocPerLanguagePerComponent( helper );
//  slocPerLanguagePerComponentModule( helper );

//  issuesPerComponent( helper );
//  issuesPerComponentModule( helper );

  issuesPerPluginComponentModule( helper );

//  slocIssuesRatioComponent( helper );
//  slocIssuesRatioComponentModule( helper );

  return 0;
}
