#!/bin/sh
# map a search maps for projects

# $1 is the component and $2 is the module

if ( test ! -d $1-api/$2-apidocs )
then
  exit 1
fi

underscorefixed=`echo $2 | sed -e 's/-/_/g'`
echo "<?php"
echo "\$${underscorefixed}_project = array("

cat $1-api/$2-apidocs/subdirs | sed -e 's/<li>.*<a href="@topdir@/'$1'-api\/'$2'-apidocs/' | sed -e 's/<\/a><\/li>//' | sed -e 's/">/@/' | awk -F@ '{printf("  \"%s\" => \"%s\",\n",$1,tolower($2))}'

echo ");"
echo "?>"
