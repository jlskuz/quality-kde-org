#!/usr/bin/env python3
# SPDX-FileCopyrightText: Friedrich W. H. Kossebau <kossebau@kde.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import argparse
import requests
import sys


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('component', help='Name of component to list repos within')

    args = parser.parse_args()

    try:
        r = requests.get('https://projects.kde.org/api/v1/find?active=true')
        allprojects = r.json()
    except Exception as exc:
        sys.exit("Failed to get list of repos from projects.kde.org: {}".format(exc))

    filtered_projects = []
    for project in allprojects:
        # whitelist
        if args.component and not project.startswith(args.component):
            continue

        filtered_projects.append(project)

    print(" ".join(filtered_projects))

if __name__ == '__main__':
    main()

