#!/bin/bash

# Written by David Faure <faure@kde.org>

# This script is called for each "version" (branch-group)

# Due to git.kolab.org
export GIT_SSL_NO_VERIFY=1

version="$1"

export PATH=$HOME/bin:$HOME/inst/bin:$PATH


# Step 1, run kdesrc-build to update the checkouts

logfile="/home/lxr/logs/src_update.$version.log"
rm -f $logfile
date > $logfile

cd ~/kdesrc-build || exit 1
git pull -q || exit 1
cmake -DCMAKE_DISABLE_FIND_PACKAGE_ECM=1 >>$logfile 2>&1 || exit 1
make install 2>&1 >>$logfile
cd


flags="--src-only"
kdesrc-build --rc-file=$version-kdesrc-buildrc $flags 2>&1 >>$logfile

grep Error $logfile

# TODO update this for KF5
# While we're here...
#~/bin/versionneddtd.sh $HOME/src/latest-qt4/kde/kdelibs/kdecore/kconfig_compiler kcfg.xsd $HOME/www/sites/www/standards/kcfg
#~/bin/versionneddtd.sh $HOME/src/latest-qt4/kde/kdelibs/kdeui/xmlgui kxmlgui.xsd $HOME/www/sites/www/standards/kxmlgui


