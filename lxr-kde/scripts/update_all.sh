#!/bin/sh

# This one is not in cron, it takes too long. Only useful for a manual full reindexing

export PATH=$HOME/bin:$PATH

update_one_version.sh kf5-qt5

update_one_version.sh stable-kf5-qt5

update_one_version.sh stable-qt4


# Now run genxref for all versions together (with reindexall == full reindex, necessary when lxr_filenum got busted)

update_genxref.sh


