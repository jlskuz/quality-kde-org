#!/bin/sh

export PATH=$HOME/bin:$PATH

reindex=
# Reindex on tuesdays
if test $(date +%u) = 2; then
  reindex=1
fi

update_one_version.sh kf5-qt5
if [ -z "$reindex" ]; then
  update_genxref.sh kf5-qt5
fi

update_one_version.sh stable-kf5-qt5
if [ -z "$reindex" ]; then
  update_genxref.sh stable-kf5-qt5
fi

if [ -n "$reindex" ]; then
  update_genxref.sh
fi

